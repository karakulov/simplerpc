# Quick examples

## server.js

```javascript
var simplerpc = require('simplerpc')

simplerpc.server({port: 1234}, { // or { port: 1234, address: '127.0.0.1' }
    test: function(a, b, callback) {
        callback(a + b)
    },
});
```

## client.js

```javascript
var simplerpc = require('simplerpc')

simplerpc.remote({port: 1234}, function(remote){ // or { port: 1234, address: '127.0.0.1' }
    remote.test(1, 2,function(res){
        console.log(res) // <= 3
        remote.end()
    })
})
```

