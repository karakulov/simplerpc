var net = require('net')

exports.server = function(listen_options, functions){
	var port = listen_options.port ? listen_options.port : -1
	var address = listen_options.address ? listen_options.address : '0.0.0.0'


	if (port == -1) throw {
			code: 'ERR_NO_PORT',
			message: 'Укажите порт для RPC сервера'
		}
	

	net.createServer(function(sock){
		var functions_init = []

		var cb_func = function(cb_indx) {
			return function(){
				var cb_pack = JSON.stringify({
					type: 'cb_function',
					indx: cb_indx,
					data: arguments
				})

				sock.write(cb_pack)
			}
		}

		for (var i in functions) {
			functions_init.push(i)
		}

		var func_init_pack = JSON.stringify({
			type: 'functions',
			data: functions_init
		})

		sock.write(func_init_pack)

		var get_data = ''
		sock.on('data', function(data){
			get_data += data.toString()

			var json_data = get_data

			try {
				var myDataPack = JSON.parse(json_data)
			} catch (e) {
				if (e instanceof SyntaxError) {
					// Если дошли не все пакеты с инфой
				} else {
					console.error(e)
					get_data = ''
					sock.write(JSON.stringify(e))
					sock.end()
				}
			}

			if (myDataPack) {
				switch(myDataPack.type){
					case 'function':
						var args = []
						var func_run = myDataPack.data.name

						for (var i in myDataPack.data.args) {
							args.push(myDataPack.data.args[i])
						}

						args.forEach(function(arg, indx){
							if (typeof arg == 'string' && arg.match(/is\_function/i)) {
								var cb_indx = arg.match(/is\_function\:(\d+)/i)[1]
								args[indx] = cb_func(cb_indx)
							}
						})

						functions[func_run].apply(this,args)
						get_data = ''
						break
				}
			}
		})

		sock.on('end', function(){
			// console.log(client + ' disconnected')
		})

		sock.on('error', function(e) {
			console.error(e)
		})
	}).listen(port, address)
}

exports.remote = function(connect_options, cb){

	connect_options.address = connect_options.address ? connect_options.address : '127.0.0.1'

	var remote = {}
	var client = net.connect(connect_options.port, connect_options.address, function(){
	var cb_functions = []

		client.on('data', function(data){
			var json_data = data.toString()

			try{
				var work_object = JSON.parse(json_data)
			} catch(e) {
				console.error(e)
				client.end()
			}

			switch(work_object.type){
				case 'cb_function':
					var arguments = work_object.data
					var args = []
					var cb_indx = work_object.indx

					for (var i in arguments) {
						args.push(arguments[i])
					}

					if (typeof cb_functions[cb_indx] == 'function') {
						cb_functions[cb_indx].apply(this, args)
						delete cb_functions[cb_indx]
					}
					break

				case 'functions': 
					remote = {}

					remote.end = function(){
						client.end()
					}

					work_object.data.forEach(function(f_name){
						remote[f_name] = function(){

							for (var i in arguments) {
								if (typeof arguments[i] == 'function') {
									cb_functions.push(arguments[i])
									var cb_indx = cb_functions.length - 1
									arguments[i] = 'is_function:' + cb_indx
								}
							}

							var pack = {
								type: 'function',
								data: {
									name: f_name,
									args: arguments
								}
							}

							client.write(new Buffer(JSON.stringify(pack)))
						}
					})

					cb(remote)
					break
			}
		})
	})
}